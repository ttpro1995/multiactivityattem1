package com.example.multiattemp1;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class FirstActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_first);
		//list view code goes here
		ListView MyList = (ListView) findViewById(R.id.listView1);
		String stringvalue[]={"6AM - Cho meo an ","8AM - Di ngu ", "11AM - day an sang"};
		
		ArrayList<String> list=new ArrayList<String>();
		for (int i=0;i<stringvalue.length;i++)
		{
			list.add(stringvalue[i]);
		}
		
		ArrayAdapter adapter=new ArrayAdapter(this,android.R.layout.simple_list_item_1,list);
		// http://www.vogella.com/tutorials/AndroidListView/article.html
		
		MyList.setAdapter(adapter);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.first, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
